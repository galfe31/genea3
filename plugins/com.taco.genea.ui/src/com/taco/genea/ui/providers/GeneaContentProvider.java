package com.taco.genea.ui.providers;

import java.io.IOException;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import com.taco.genea.Registry;
import com.taco.genea.provider.GeneaItemProviderAdapterFactory;


public class GeneaContentProvider implements ITreeContentProvider, IResourceChangeListener {

	GeneaItemProviderAdapterFactory factory;
	AdapterFactoryContentProvider adaptContentFactory;
	private Viewer viewer;

	public GeneaContentProvider() {
		factory = new GeneaItemProviderAdapterFactory();
		adaptContentFactory = new AdapterFactoryContentProvider(factory);
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public Object[] getChildren(Object parentElement) {

		if (parentElement instanceof org.eclipse.core.internal.resources.File) {
			org.eclipse.core.internal.resources.File eFile = (org.eclipse.core.internal.resources.File) parentElement;

			if (eFile.getFileExtension().equals("cosmos")) {

				ResourceSet rset = new ResourceSetImpl();
				String fpath = eFile.getLocation().toString();
				URI uri = URI.createURI("file:/" + fpath);
				Resource r = rset.createResource(uri);
				try {
					r.load(null);
					Registry reg = (Registry) r.getContents().get(0);
					return adaptContentFactory.getChildren(reg);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (factory.isFactoryForType(parentElement)) {
			return adaptContentFactory.getChildren(parentElement);
		}
		return new Object[] {};
	}

	@Override
	public Object getParent(Object element) {
		return adaptContentFactory.getParent(element);
	}

	@Override
	public boolean hasChildren(Object element) {
		Object[] result = getElements(element);
		return result.length > 0;
	}

	@Override
	public void dispose() {
		ITreeContentProvider.super.dispose();

	}

	@Override
	public void inputChanged(Viewer v, Object old, Object noo) {
		this.viewer = v;
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
			if (viewer != null && viewer.getControl()!=null && !viewer.getControl().isDisposed()) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						viewer.refresh();
					}
				});
				
			}


	}

}
