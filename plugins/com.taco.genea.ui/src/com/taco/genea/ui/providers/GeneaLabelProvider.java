package com.taco.genea.ui.providers;

import java.util.List;

import org.eclipse.core.internal.resources.File;
import org.eclipse.core.internal.resources.Folder;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.taco.genea.provider.GeneaItemProviderAdapterFactory;


public class GeneaLabelProvider implements ILabelProvider {
	
	private ImageRegistry imgReg;
	public static final String PROJECT_IMG = "icons/road-icon-19714.png";
	public static final String FOLDER_IMG = "icons/folder-icon-55848.png";
	public static final String FILE_IMG = "icons/document-blank-icon-6947.png";
	public static final String COSMOS_FILE = "icons/CatalogModelFile.gif";
	public static final String DEFAULT_IMG = "icons/cross-symbol-icon-6388.png";
	public static final String ACTIVE_ECORE_FILE_IMG = "icons/CoSMoS.gif";
	public static final String ACTIVE_PROJECT_VERSION_IMG = "icons/small-red-circle-icon-57555.png";
	public static final String ACTIVE_CUSTOMER_IMG = "icons/shield-star-icon-3117.png";
	public static final String ACTIVE_PROJECT_ENTRY_IMG = "icons/box-icon-7281.png";
	public static final String CUSTOMER_IMG = "icons/car_1_16_16.gif";
	public static final String PROJECT_ENTRY_IMG = "icons/box-icon-55906.png";
	public static final String PROJECT_VERSION_IMG = "icons/small-briefcase-icon-22188.png";
		
	GeneaItemProviderAdapterFactory factory;
	AdapterFactoryLabelProvider adaptLabelFactory;
//	ICosmosWorkspace cws;
	
	public GeneaLabelProvider() {
		factory = new GeneaItemProviderAdapterFactory();
		adaptLabelFactory = new AdapterFactoryLabelProvider(factory);
//		cws = new CosmosWorkspaceImpl();
		Bundle b = FrameworkUtil.getBundle(this.getClass());
		
		imgReg = new ImageRegistry();
		imgReg.put(FILE_IMG, ImageDescriptor.createFromURL(b.getEntry(FILE_IMG)));
		imgReg.put(FOLDER_IMG, ImageDescriptor.createFromURL(b.getEntry(FOLDER_IMG)));
		imgReg.put(PROJECT_IMG, ImageDescriptor.createFromURL(b.getEntry(PROJECT_IMG)));
		imgReg.put(DEFAULT_IMG, ImageDescriptor.createFromURL(b.getEntry(DEFAULT_IMG)));
		imgReg.put(COSMOS_FILE, ImageDescriptor.createFromURL(b.getEntry(COSMOS_FILE)));
		imgReg.put(ACTIVE_ECORE_FILE_IMG, ImageDescriptor.createFromURL(b.getEntry(ACTIVE_ECORE_FILE_IMG)));
		imgReg.put(ACTIVE_PROJECT_VERSION_IMG, ImageDescriptor.createFromURL(b.getEntry(ACTIVE_PROJECT_VERSION_IMG)));
		imgReg.put(ACTIVE_CUSTOMER_IMG, ImageDescriptor.createFromURL(b.getEntry(ACTIVE_CUSTOMER_IMG)));
		imgReg.put(ACTIVE_PROJECT_ENTRY_IMG, ImageDescriptor.createFromURL(b.getEntry(ACTIVE_PROJECT_ENTRY_IMG)));
		imgReg.put(CUSTOMER_IMG, ImageDescriptor.createFromURL(b.getEntry(CUSTOMER_IMG)));
		imgReg.put(PROJECT_ENTRY_IMG, ImageDescriptor.createFromURL(b.getEntry(PROJECT_ENTRY_IMG)));
		imgReg.put(PROJECT_VERSION_IMG,ImageDescriptor.createFromURL(b.getEntry(PROJECT_VERSION_IMG)));
	}
	
	@Override
	public void addListener(ILabelProviderListener listener) {
		adaptLabelFactory.addListener(listener);
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return adaptLabelFactory.isLabelProperty(element, property);
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		adaptLabelFactory.removeListener(listener);
		
	}

	@Override
	public Image getImage(Object element) {

		if(element instanceof  org.eclipse.core.internal.resources.Project) {
			return imgReg.get(PROJECT_IMG);
		}
		if(element instanceof org.eclipse.core.internal.resources.Folder) {
			IFolder fold = (IFolder)element;
			String s = fold.getName();
//			ICustomer cust = cws.getCustomer(s);
//			if(cust!=null) {
//				if(cust.isActive()) {
//					return imgReg.get(ACTIVE_CUSTOMER_IMG);
//				}
//				else {
//					return imgReg.get(CUSTOMER_IMG);
//				}
//			}
			IContainer parent = fold.getParent();
//			ICosmosProjectEntry pentry = cws.getProjectEntry(parent.getName(), fold.getName());
//			if(pentry!=null) {
//				if(pentry.isActive()) {
//					return imgReg.get(ACTIVE_PROJECT_ENTRY_IMG);
//				}
//				else {
//					return imgReg.get(PROJECT_ENTRY_IMG);
//				}
//				
//			}
//			IContainer grandParent = parent.getParent();
//			pentry = cws.getProjectEntry(grandParent.getName(), parent.getName());
//
//			if(pentry!=null) {
//				List<ICosmosProjectVersion> versions = pentry.getVersions();
//				for(ICosmosProjectVersion pv:versions) {
//	
//					if(pv.getFolderName().trim().equals(fold.getName().trim())) {
//						
//						if(pv.isActive()) {
//							return imgReg.get(ACTIVE_PROJECT_VERSION_IMG);
//						}
//						return imgReg.get(PROJECT_VERSION_IMG);
//					}
//				}
//			}
			return imgReg.get(FOLDER_IMG);
		}
		
		
		
		
		if(element instanceof org.eclipse.core.internal.resources.File) {
			File eFile = (File)element;
			if(eFile.getName().endsWith(".genea")) {
//				Project p = CosmosWorkspaceUtil.loadProject(eFile);
//				if(p.isWriteEnabled()) {
//					return imgReg.get(ACTIVE_ECORE_FILE_IMG);
//				}
				return imgReg.get(COSMOS_FILE);
			}
			return imgReg.get(FILE_IMG);
		}
		
		return adaptLabelFactory.getImage(element);
	}

	@Override
	public String getText(Object element) {

		if(element instanceof File) {
			File eFile = (File)element;
			return eFile.getName();
		}
		if(element instanceof Folder) {
			Folder eFolder = (Folder)element;
			return eFolder.getName();
		}
		if(element instanceof IProject) {
			IProject eProject = (IProject)element;
			return eProject.getName();
		}
		return adaptLabelFactory.getText(element);
	}	



	@Override
	public void dispose() {
		imgReg.dispose();

	}



}
