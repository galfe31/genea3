package com.taco.genea;

import com.taco.genea.impl.GeneaFactoryImpl;

/** This factory  overrides the generated factory and returns the new generated interfaces */
public interface GeneaFactory extends MGeneaFactory 
{
	
	/** Specialize the eINSTANCE initialization with the new interface type 
	  * (overridden in the override_factory extension)
	*/
	GeneaFactory eINSTANCE = GeneaFactoryImpl.init();
				
}
