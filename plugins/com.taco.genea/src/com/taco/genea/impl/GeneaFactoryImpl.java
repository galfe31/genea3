package com.taco.genea.impl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import com.taco.genea.GeneaFactory;


// This factory  renames the generated factory interface to use it as an overriden factory
public class GeneaFactoryImpl extends MGeneaFactoryImpl implements GeneaFactory
{
	
	public static GeneaFactory init() {
		
		try {
			Object fact = MGeneaFactoryImpl.init();
			if ((fact != null) && (fact instanceof GeneaFactory))
					return (GeneaFactory) fact;
			}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GeneaFactoryImpl(); 
		 }
	

}
