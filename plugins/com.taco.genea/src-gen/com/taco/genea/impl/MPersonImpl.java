/**
 */
package com.taco.genea.impl;

import com.taco.genea.Adress;
import com.taco.genea.MGeneaPackage;
import com.taco.genea.Person;
import com.taco.genea.Story;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getBirth <em>Birth</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getDeath <em>Death</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getAdress <em>Adress</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getStories <em>Stories</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getFather <em>Father</em>}</li>
 *   <li>{@link com.taco.genea.impl.MPersonImpl#getMother <em>Mother</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MPersonImpl extends MinimalEObjectImpl.Container implements Person {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected String firstName = FIRST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getBirth() <em>Birth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirth()
	 * @generated
	 * @ordered
	 */
	protected static final Date BIRTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBirth() <em>Birth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirth()
	 * @generated
	 * @ordered
	 */
	protected Date birth = BIRTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeath() <em>Death</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeath()
	 * @generated
	 * @ordered
	 */
	protected static final Date DEATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeath() <em>Death</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeath()
	 * @generated
	 * @ordered
	 */
	protected Date death = DEATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAdress() <em>Adress</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdress()
	 * @generated
	 * @ordered
	 */
	protected Adress adress;

	/**
	 * The cached value of the '{@link #getStories() <em>Stories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStories()
	 * @generated
	 * @ordered
	 */
	protected EList<Story> stories;

	/**
	 * The cached value of the '{@link #getFather() <em>Father</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFather()
	 * @generated
	 * @ordered
	 */
	protected Person father;

	/**
	 * The cached value of the '{@link #getMother() <em>Mother</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMother()
	 * @generated
	 * @ordered
	 */
	protected Person mother;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MGeneaPackage.Literals.PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFirstName() {
		return firstName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFirstName(String newFirstName) {
		String oldFirstName = firstName;
		firstName = newFirstName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__FIRST_NAME, oldFirstName,
					firstName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getBirth() {
		return birth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBirth(Date newBirth) {
		Date oldBirth = birth;
		birth = newBirth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__BIRTH, oldBirth, birth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getDeath() {
		return death;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDeath(Date newDeath) {
		Date oldDeath = death;
		death = newDeath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__DEATH, oldDeath, death));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adress getAdress() {
		return adress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdress(Adress newAdress, NotificationChain msgs) {
		Adress oldAdress = adress;
		adress = newAdress;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__ADRESS,
					oldAdress, newAdress);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAdress(Adress newAdress) {
		if (newAdress != adress) {
			NotificationChain msgs = null;
			if (adress != null)
				msgs = ((InternalEObject) adress).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MGeneaPackage.PERSON__ADRESS, null, msgs);
			if (newAdress != null)
				msgs = ((InternalEObject) newAdress).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MGeneaPackage.PERSON__ADRESS, null, msgs);
			msgs = basicSetAdress(newAdress, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__ADRESS, newAdress, newAdress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Story> getStories() {
		if (stories == null) {
			stories = new EObjectResolvingEList<Story>(Story.class, this, MGeneaPackage.PERSON__STORIES);
		}
		return stories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person getFather() {
		if (father != null && father.eIsProxy()) {
			InternalEObject oldFather = (InternalEObject) father;
			father = (Person) eResolveProxy(oldFather);
			if (father != oldFather) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MGeneaPackage.PERSON__FATHER, oldFather,
							father));
			}
		}
		return father;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person basicGetFather() {
		return father;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFather(Person newFather) {
		Person oldFather = father;
		father = newFather;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__FATHER, oldFather, father));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person getMother() {
		if (mother != null && mother.eIsProxy()) {
			InternalEObject oldMother = (InternalEObject) mother;
			mother = (Person) eResolveProxy(oldMother);
			if (mother != oldMother) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MGeneaPackage.PERSON__MOTHER, oldMother,
							mother));
			}
		}
		return mother;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person basicGetMother() {
		return mother;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMother(Person newMother) {
		Person oldMother = mother;
		mother = newMother;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MGeneaPackage.PERSON__MOTHER, oldMother, mother));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MGeneaPackage.PERSON__ADRESS:
			return basicSetAdress(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MGeneaPackage.PERSON__NAME:
			return getName();
		case MGeneaPackage.PERSON__FIRST_NAME:
			return getFirstName();
		case MGeneaPackage.PERSON__BIRTH:
			return getBirth();
		case MGeneaPackage.PERSON__DEATH:
			return getDeath();
		case MGeneaPackage.PERSON__ADRESS:
			return getAdress();
		case MGeneaPackage.PERSON__STORIES:
			return getStories();
		case MGeneaPackage.PERSON__FATHER:
			if (resolve)
				return getFather();
			return basicGetFather();
		case MGeneaPackage.PERSON__MOTHER:
			if (resolve)
				return getMother();
			return basicGetMother();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MGeneaPackage.PERSON__NAME:
			setName((String) newValue);
			return;
		case MGeneaPackage.PERSON__FIRST_NAME:
			setFirstName((String) newValue);
			return;
		case MGeneaPackage.PERSON__BIRTH:
			setBirth((Date) newValue);
			return;
		case MGeneaPackage.PERSON__DEATH:
			setDeath((Date) newValue);
			return;
		case MGeneaPackage.PERSON__ADRESS:
			setAdress((Adress) newValue);
			return;
		case MGeneaPackage.PERSON__STORIES:
			getStories().clear();
			getStories().addAll((Collection<? extends Story>) newValue);
			return;
		case MGeneaPackage.PERSON__FATHER:
			setFather((Person) newValue);
			return;
		case MGeneaPackage.PERSON__MOTHER:
			setMother((Person) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MGeneaPackage.PERSON__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MGeneaPackage.PERSON__FIRST_NAME:
			setFirstName(FIRST_NAME_EDEFAULT);
			return;
		case MGeneaPackage.PERSON__BIRTH:
			setBirth(BIRTH_EDEFAULT);
			return;
		case MGeneaPackage.PERSON__DEATH:
			setDeath(DEATH_EDEFAULT);
			return;
		case MGeneaPackage.PERSON__ADRESS:
			setAdress((Adress) null);
			return;
		case MGeneaPackage.PERSON__STORIES:
			getStories().clear();
			return;
		case MGeneaPackage.PERSON__FATHER:
			setFather((Person) null);
			return;
		case MGeneaPackage.PERSON__MOTHER:
			setMother((Person) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MGeneaPackage.PERSON__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MGeneaPackage.PERSON__FIRST_NAME:
			return FIRST_NAME_EDEFAULT == null ? firstName != null : !FIRST_NAME_EDEFAULT.equals(firstName);
		case MGeneaPackage.PERSON__BIRTH:
			return BIRTH_EDEFAULT == null ? birth != null : !BIRTH_EDEFAULT.equals(birth);
		case MGeneaPackage.PERSON__DEATH:
			return DEATH_EDEFAULT == null ? death != null : !DEATH_EDEFAULT.equals(death);
		case MGeneaPackage.PERSON__ADRESS:
			return adress != null;
		case MGeneaPackage.PERSON__STORIES:
			return stories != null && !stories.isEmpty();
		case MGeneaPackage.PERSON__FATHER:
			return father != null;
		case MGeneaPackage.PERSON__MOTHER:
			return mother != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", firstName: ");
		result.append(firstName);
		result.append(", birth: ");
		result.append(birth);
		result.append(", death: ");
		result.append(death);
		result.append(')');
		return result.toString();
	}

} //MPersonImpl
