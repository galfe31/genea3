/**
 */
package com.taco.genea;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.taco.genea.MPerson#getName <em>Name</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getFirstName <em>First Name</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getBirth <em>Birth</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getDeath <em>Death</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getAdress <em>Adress</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getStories <em>Stories</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getFather <em>Father</em>}</li>
 *   <li>{@link com.taco.genea.MPerson#getMother <em>Mother</em>}</li>
 * </ul>
 *
 * @see com.taco.genea.MGeneaPackage#getPerson()
 * @model
 * @generated
 */
public interface MPerson extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see com.taco.genea.MGeneaPackage#getPerson_FirstName()
	 * @model
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Birth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Birth</em>' attribute.
	 * @see #setBirth(Date)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Birth()
	 * @model
	 * @generated
	 */
	Date getBirth();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getBirth <em>Birth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Birth</em>' attribute.
	 * @see #getBirth()
	 * @generated
	 */
	void setBirth(Date value);

	/**
	 * Returns the value of the '<em><b>Death</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Death</em>' attribute.
	 * @see #setDeath(Date)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Death()
	 * @model
	 * @generated
	 */
	Date getDeath();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getDeath <em>Death</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Death</em>' attribute.
	 * @see #getDeath()
	 * @generated
	 */
	void setDeath(Date value);

	/**
	 * Returns the value of the '<em><b>Adress</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adress</em>' containment reference.
	 * @see #setAdress(Adress)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Adress()
	 * @model containment="true"
	 * @generated
	 */
	Adress getAdress();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getAdress <em>Adress</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adress</em>' containment reference.
	 * @see #getAdress()
	 * @generated
	 */
	void setAdress(Adress value);

	/**
	 * Returns the value of the '<em><b>Stories</b></em>' reference list.
	 * The list contents are of type {@link com.taco.genea.Story}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stories</em>' reference list.
	 * @see com.taco.genea.MGeneaPackage#getPerson_Stories()
	 * @model
	 * @generated
	 */
	EList<Story> getStories();

	/**
	 * Returns the value of the '<em><b>Father</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Father</em>' reference.
	 * @see #setFather(Person)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Father()
	 * @model
	 * @generated
	 */
	Person getFather();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getFather <em>Father</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Father</em>' reference.
	 * @see #getFather()
	 * @generated
	 */
	void setFather(Person value);

	/**
	 * Returns the value of the '<em><b>Mother</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mother</em>' reference.
	 * @see #setMother(Person)
	 * @see com.taco.genea.MGeneaPackage#getPerson_Mother()
	 * @model
	 * @generated
	 */
	Person getMother();

	/**
	 * Sets the value of the '{@link com.taco.genea.MPerson#getMother <em>Mother</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mother</em>' reference.
	 * @see #getMother()
	 * @generated
	 */
	void setMother(Person value);

} // MPerson
