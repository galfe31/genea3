/**
 */
package com.taco.genea;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.taco.genea.MRegistry#getStories <em>Stories</em>}</li>
 *   <li>{@link com.taco.genea.MRegistry#getPersons <em>Persons</em>}</li>
 * </ul>
 *
 * @see com.taco.genea.MGeneaPackage#getRegistry()
 * @model
 * @generated
 */
public interface MRegistry extends EObject {
	/**
	 * Returns the value of the '<em><b>Stories</b></em>' containment reference list.
	 * The list contents are of type {@link com.taco.genea.Story}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stories</em>' containment reference list.
	 * @see com.taco.genea.MGeneaPackage#getRegistry_Stories()
	 * @model containment="true"
	 * @generated
	 */
	EList<Story> getStories();

	/**
	 * Returns the value of the '<em><b>Persons</b></em>' containment reference list.
	 * The list contents are of type {@link com.taco.genea.Person}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persons</em>' containment reference list.
	 * @see com.taco.genea.MGeneaPackage#getRegistry_Persons()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getPersons();

} // MRegistry
