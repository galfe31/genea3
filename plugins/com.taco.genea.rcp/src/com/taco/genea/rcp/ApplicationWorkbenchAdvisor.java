package com.taco.genea.rcp;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	//private static final String PERSPECTIVE_ID = "com.taco.genea.rcp.perspective"; //$NON-NLS-1$
	private static final String PERSPECTIVE_ID = "com.taco.genea.ui.perspective.main"; //$NON-NLS-1$
	@Override
    public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        return new ApplicationWorkbenchWindowAdvisor(configurer);
    }
	
	@Override
	public IAdaptable getDefaultPageInput() {
		IWorkspaceRoot ws = ResourcesPlugin.getWorkspace().getRoot();
		return ResourcesPlugin.getWorkspace().getRoot();
	}
    
    @Override
	public String getInitialWindowPerspectiveId() {
		return PERSPECTIVE_ID;
	}
}
